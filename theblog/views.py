
from this import d
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Category, Post, Comment
from .forms import PostForm, CommentForm
from django.urls import reverse_lazy
from theblog.serializer import PostSerializer, UserSerializer, CategorySerializer, ApvSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import generics
from rest_framework import mixins
from django.http import HttpResponse
from django.contrib.auth.models import User
from rest_framework import permissions
from theblog.permissions import IsAuthorOrReadOnly
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser


# Create your views here.
class UserList(generics.ListAPIView):
    queryset= User.objects.all()
    serializer_class= UserSerializer

class UserDetail(generics.RetrieveAPIView):
    queryset= User.objects.all()
    serializer_class= UserSerializer


class CategoryViewAPI(APIView):

    authentication_classes=[BasicAuthentication]
    permission_classes=[IsAdminUser]

    def get(self, request):
        category= Category.objects.all()
        serializer= CategorySerializer(category, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer= CategorySerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  
    
    def delete(self, request, id):
    
        category = self.get_object(id)
        category.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

"""
class ApprovalViewAPI(APIView):

    def get(self, request):
        approvaldata= Post.approved.objects.all()
        serializer = ApprovalSerializer(approvaldata, many=True).data
        return Response(serializer.data)

"""
class ApvView(APIView):
    authentication_classes=[BasicAuthentication]
    permission_classes=[IsAdminUser]
    

    def get_object(self):
        try:
            return Post.objects.get()

        except Post.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
    
    def get(self, request):
        posts= Post.objects.all()
        serializer= ApvSerializer(posts, many=True)
        return Response(serializer.data)


    def put(self, request):
        
        serializer = ApvSerializer(data= request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
   
class PostView(APIView):
    authentication_classes= [SessionAuthentication, BasicAuthentication]
    permission_classes= [permissions.IsAuthenticatedOrReadOnly, IsAuthorOrReadOnly]
    
    
    def get(self, request):
        posts= Post.objects.all()
        serializer= PostSerializer(posts, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer= PostSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

class PostDetailAPI(APIView):
    authentication_classes= [SessionAuthentication, BasicAuthentication]
    permission_classes= [permissions.IsAuthenticatedOrReadOnly, IsAuthorOrReadOnly]


    def get_object(self, id):
        try:
            return Post.objects.get(id=id)

        except Post.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        post = self.get_object(id)
        serializer = PostSerializer(post)
        return Response(serializer.data)

    def put(self, request, id):
        post = self.get_object(id)
        serializer = PostSerializer(post, data= request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
    
        post = self.get_object(id)
        post.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


#def home(request):
#    return render(request,'home.html', {})

def admin_approval(request):
    post_list = Post.objects.all().order_by('-post_date')
    if request.user.is_superuser:
        if request.method == "POST":
            id_list = request.POST.getlist('boxes')
            
            post_list.update(approved=False)


            for x in id_list:
                Post.objects.filter(pk=int(x)).update(approved=True)

            messages.success(request, ("Post Approval Updated"))
            return redirect('home')
        else:

            return render(request, 'admin_approval.html',
            {"post_list": post_list})
    else:
        messages.success(request, ("You cannot access this page"))
        return redirect('home')

    return render(request, 'admin_approval.html')

class HomeView(ListView):
    model = Post
    template_name = 'home.html'
    ordering = ['-post_date']
    #ordering = ['-id']

    def get_context_data(self, *args, **kwargs):
        cat_menu = Category.objects.all()
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        context["cat_menu"] = cat_menu
        return context

def CategoryView(request, cats):
    category_posts = Post.objects.filter(category= cats.replace('-', ' '))
    return render(request, 'categories.html', {'cats':cats.replace('-', ' ').title(), 'category_posts':category_posts})

class ArticleDetailView(DetailView):
    model = Post
    template_name = 'article_details.html'

    def get_context_data(self, *args, **kwargs):
        cat_menu = Category.objects.all()
        context = super(ArticleDetailView, self).get_context_data(*args, **kwargs)
        context["cat_menu"] = cat_menu
        return context

class AddPostView(CreateView):
    model = Post
    form_class = PostForm
    template_name = 'add_post.html'
    #fields = '__all__'

class AddCommentView(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'add_comment.html'
    #fields= '__all__'
    def form_valid(self, form):
        form.instance.post_id = self.kwargs['pk']
        return super().form_valid(form)
        
    success_url = reverse_lazy('home')

class AddCategoryView(CreateView):
    model = Category
    #form_class = PostForm
    template_name = 'add_category.html'
    fields = '__all__'

class UpdatePostView(UpdateView):
    model = Post 
    form_class = PostForm
    template_name= 'update_post.html'
    #fields = ['title', 'title_tag', 'body']


class DeletePostView(DeleteView):
    model = Post
    template_name = 'delete_post.html'
    success_url = reverse_lazy('home')