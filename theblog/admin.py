from django.contrib import admin
from .models import Post, Category, Comment
# Register your models here.


admin.site.register(Category)

admin.site.register(Comment) 

class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'approved']
    ordering = ['title']

admin.site.register(Post, PostAdmin)
