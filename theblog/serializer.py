from rest_framework import serializers
from theblog.models import Post, Category
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    posts= serializers.PrimaryKeyRelatedField(many=True, queryset=Post.objects.all())
    author= serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = User
        fields= ['id', 'username', 'posts', 'author','approved']

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model= Post
        fields= ['id','title','author','category']

class ApvSerializer(serializers.ModelSerializer):
    class Meta:
        model= Post
        fields= ['id', 'title', 'approved']

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model= Category
        fields= ['name']

"""
class ApprovalSerializer(serializers.Serializer):
   
   approval = serializers.BooleanField()
"""  