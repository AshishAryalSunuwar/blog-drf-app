from django.urls import path
from . import views
from .views import HomeView, ArticleDetailView, AddPostView, UpdatePostView, DeletePostView, AddCategoryView, CategoryView, AddCommentView, PostView, PostDetailAPI, CategoryViewAPI, ApvView
from theblog import views
from django.urls import include, path


urlpatterns = [
    #path('', views.home, name="home"),
    path('', HomeView.as_view(), name='home'),
    path('article/<int:pk>', ArticleDetailView.as_view(), name= 'article-detail'),
    path('add_post/', AddPostView.as_view(), name='add_post'),
    path('add_category/', AddCategoryView.as_view(), name='add_category'),
    path('article/edit/<int:pk>', UpdatePostView.as_view(), name= 'update_post'),
    path('article/<int:pk>/remove', DeletePostView.as_view(), name= 'delete_post'),
    path('category/<str:cats>/', CategoryView, name='category'),
    path('article/<int:pk>/comment/', AddCommentView.as_view(), name='add_comment'),
    path('admin_approval', views.admin_approval, name='admin_approval'),
    path('drf', PostView.as_view(), name='drfapi'),    
    path('detaildrf/<int:id>/', PostDetailAPI.as_view(), name='drfdetail'),    
    path('users/', views.UserList.as_view()),
    path('users/<int:id>/', views.UserDetail.as_view()),    
    path('catdrf', CategoryViewAPI.as_view(), name='catapi'),
   # path('approvaldrf', ApprovalViewAPI.as_view(), name='approvalapi'),
    path('apvdrf/', ApvView.as_view(), name='apvapi'),    


]
